# android-ci

[![pipeline status](https://gitlab.com/optimisedlabs/android-ci/badges/master/pipeline.svg)](https://gitlab.com/optimisedlabs/android-ci/commits/master)

## Purpose

Permits the building of Android projects in an GitLab CI environment.

Whilst looking around for a suitable container I found [android-ci](https://gitlab.com/showcheap/android-ci), however, I discounted this project as it didn't have the appropraite Android tools version we were using at the time `26.0.2` as a tagged release. I put it here for reference as it's `Dockerfile` is worth re-considering as a basis!

Ultimately I chose to base our `Dockerfile` on the [gitlab-ci-android](https://github.com/jangrewe/gitlab-ci-android), which itself either pulls heavily on the above project or they have another shared heritage. I preferred this project as the `packages.txt` was seperated out.

Our image extends these by making use of [fastlane](https://fastlane.tools). Although it does not yet follow the advice in their [Continuous Integration](https://docs.fastlane.tools/best-practices/continuous-integration/#gitlab-ci-integration) section. Highlevel of changes from original `Dockerfile`:

```bash
apt-get update && apt-get install -y build-essential ruby-full
gem install fastlane --no-ri --no-rdoc
fastlane build
```

## Usage

Each Android project needs to be setup to use the Gradle wrapper. This process loosely involves:
1. Running the graddle/wrapper task found in Android Studio
2. Committing the output `gradle/wrapper/graddle-wrapper.js` file, see #7
3. Revert any other changes (eg, # header in `gradlew.sh`)
4. Copy the latest template files for Android from our monorepo. No tweaks should be necessary to these files, if so, consider contributing back to the template(s)!

### Manual CI test

Use the command `fastlane build` from a command prompt when insde the project on your machine. This will build both the debug and release builds. *NOTE*: The release build is not signed.

### Day-to-day Continuous Integration

By default any branch will perform a `fastlane build` when pushed. The results of the build will be posted in the Slack channel related to the project.

### Owner / Product Manager / Release Manager / etc...

The time has arrived to get a build into Google Play / HockeyApp for distribution 🚀. Thankfully this process is straightforward, but the result will need to be [manually uploaded](https://gitlab.com/optimisedlabs/android-ci/issues/6).

1. Open a command prompt in the project folder
2. Ensure required changes have been merged into master
3. Run `fastlane deploy`
4. When the CI server has finished the create step, the artefacts can then be manually uploaded to Google Play

## Signing release builds

Any release that is installed by and end-user, should be correctly signed.

### Setup
This section assumes:
* that signing needs to be setup
* you have master access to the project
* you have the relevant knowledge to get files in/out of a Docker container
* your project is already using the latest OL template for Android

1. Start a container using `docker run -it --rm registry.gitlab.com/optimisedlabs/android-ci:v0.2 bash`
2. Use 1Password to generate two 64 character random passwords. One will be used for the keystore password and the other for the key password.
3. Generate / validate the following information that will be asked in the next step:
  * Keystore Name: <sensible filename that reflects it's purpose, eg, ol-bws-myehcp.jks>
  * Alias: <name of app, in lowercase, eg, myehcp>
  * First/last name: Optimised Labs
  * Organisational Unit: <name of app using mixed case, eg, MyEHCP>
  * Organisation: Optimised Labs Ltd
  * City: Plymouth
  * State: Devon
  * Country: UK
4. Create a 'Java Key Store' using the following command entering the details generated/collected above:
    `keytool -genkey -v -keystore <keystore name>.jks -keyalg RSA -keysize 2048 -validity 10000 -alias <alias>`
5. Base64 encode the keystore using `base64 <keystore name>.jks` and setup a **protected** secret variable named `KS` on the project.
6. Add a **protected** secret variable containing the 'keystore password' named `KS_PASSWORD`
7. Add a **protected** secret variable containing the 'key password' name `KEY_PASSWORD`
8. Ensure that all *tags* are protected using the wildcard `*`

Now each time you tag a commit it will be signed, ready to be [manually uploaded](https://gitlab.com/optimisedlabs/android-ci/issues/6).

### Manually signing

1. Start a container using `docker run -it --rm registry.gitlab.com/optimisedlabs/android-ci:v0.2 bash`
2. You can check the current signature using the command `$ANDROID_HOME/build-tools/26.0.2/apksigner verify -v --print-certs <apk>`. The output should be similar to:

    ```
    DOES NOT VERIFY
    ERROR: No JAR signatures
    ```
3. Manually sign using the command `$ANDROID_HOME/build-tools/26.0.2/apksigner sign --ks <keystore name>.jks --out app-release.apk <unsigned, but zipaligned apk>`. You will need to manually enter the password from 1Password as generated for the keystore/key above.
4. You can now verify the signature of the APK again and should see something similar to the following:

    ```
    Verifies
    Verified using v1 scheme (JAR signing): true
    Verified using v2 scheme (APK Signature Scheme v2): true
    Number of signers: 1
    Signer #1 certificate DN: CN=Optimised Labs, OU=MyEHCP, O=Optimised Labs Ltd, L=Plymouth, ST=Devon, C=UK
    Signer #1 certificate SHA-256 digest: 79b5d834cf1a6ed745552eb86f701909e39647d6a649a5b8de04767fd1554bd0
    Signer #1 certificate SHA-1 digest: 7098b32b735aa458baf7465d5ec80ece30fac76e
    Signer #1 certificate MD5 digest: dd0229ea3fc50e5f2e9d063648407334
    Signer #1 key algorithm: RSA
    Signer #1 key size (bits): 2048
    Signer #1 public key SHA-256 digest: a45020e0227918336f70ccc6c07b696752d3d429d63902e9766b62eaa05ee699
    Signer #1 public key SHA-1 digest: 31a5fa4999295d1e45e734d86fb9ec42ffa2dfe3
    Signer #1 public key MD5 digest: 0780da3fd733c3727f3538de5f7d3f15
    WARNING: META-INF/kotlin-runtime.kotlin_module not protected by signature. Unauthorized modifications to this JAR entry will not be detected. Delete or move the entry outside of META-INF/.
    WARNING: META-INF/kotlin-stdlib-jre7.kotlin_module not protected by signature. Unauthorized modifications to this JAR entry will not be detected. Delete or move the entry outside of META-INF/.
    WARNING: META-INF/kotlin-stdlib.kotlin_module not protected by signature. Unauthorized modifications to this JAR entry will not be detected. Delete or move the entry outside of META-INF/.
    ```

## GitLab Runners

Our current staging environment does not have enough memory to build Android projects (thanks Java), so you need to enable the shared GitLab Runners.

I ran a few tests and found:

### 2GB instance
* 1st run (pull deps) - 8m 43s and Gradle was killed...

        What went wrong:
        Gradle build daemon disappeared unexpectedly (it may have been killed or may have crashed)
* 2nd run, failed for same reason after 2m 51s

### 4GB instance
* 1st run (warm up) - 3m 29s
* 2nd run - 2m 30s
* 3rd run - 2m 26s

## Further Research

Plenty of room for improvements still exist. Firstly, please see [issues](https://gitlab.com/optimisedlabs/android-ci/issues), but then also:
* [Setting Up Android Builds in Gitlab CI Using Shared Runners](http://www.greysonparrelli.com/post/setting-up-android-builds-in-gitlab-ci-using-shared-runners/)
* Investigate using [openjdk:8-jdk](https://hub.docker.com/_/openjdk/) as a starting image as it claims to be ~90MB
* [Generating Android build with Gitlab CI](https://stackoverflow.com/questions/24942751/generating-android-build-with-gitlab-ci)
* [How to test and build your Android applications using Gitlab CI](https://gitlab.com/gitlab-com/blog-posts/issues/28)
* [Syncthing](https://github.com/syncthing/syncthing-android)
* [Google Play App Signing](https://developer.android.com/studio/publish/app-signing.html#google-play-app-signing)
* [zipalign](https://developer.android.com/studio/command-line/zipalign.html)
* [apksigner](https://developer.android.com/studio/command-line/apksigner.html)
